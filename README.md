# Suckless configuration files
My minimal configuration of [dwm](https://dwm.suckless.org/) + [st](https://st.suckless.org/) + [dmenu](https://tools.suckless.org/dmenu/)
## dwm patches:

 + dwm-colorbar-6.2.diff
 + dwm-deck-6.2.diff
 + dwm-removeborder-20200520-f09418b.diff
 + dwm-rotatestack-20161021-ab9571b.diff
 + shiftview.c

## st patches:

 + st-alpha-0.8.2.diff
 + st-font2-20190416-ba72400.diff
 + st-ligatures-alpha-scrollback-20200430-0.8.3.diff
 + st-scrollback-20200419-72e3f6c.diff
 + st-scrollback-mouse-20191024-a2c479c.diff
 + st-scrollback-mouse-altscreen-20200416-5703aa0.diff
 + st-vertcenter-20180320-6ac8c8a.diff
 + st-workingdir-20200317-51e19ea.diff

## dmenu patches:

 + dmenu-mousesupport-5.0.diff
